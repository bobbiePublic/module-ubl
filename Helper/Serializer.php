<?php

namespace Bobbie\UBL\Helper;

use Bobbie\XSD_to_PHP\Helper\AbstractSerializer;
use JMS\Serializer\Serializer as JMSSerializer;

class Serializer extends AbstractSerializer
{
    protected JMSSerializer $jmsSerializer;

    const YML_BASE_PATH = __DIR__ . '/../schemas/generated/jms/';
    const YML_DIRS_UBL = [ 'UN_CEFACT', 'cac', 'cbc', 'cec', 'csc', 'Qualified', 'Unqualified' ];
    const ROOT_MAP_UBL = [
        'cac' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
        'cbc' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
        'cec' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
        'csc' => 'urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2'
    ];

    const ROOT_NODE_MAP = [
        'DespatchAdvice' => 'UBL\DespatchAdvice',
	'ReceiptAdvice' => 'UBL\ReceiptAdvice'
    ];

    public function getSerializer() : JMSSerializer
    {
        if (!isset($this->jmsSerializer)) {
            $mapping = [];
            foreach (self::YML_DIRS_UBL as $dir) {
                $mapping['UBL\\' . $dir] = self::YML_BASE_PATH . 'UBL/' . $dir;
            }
            $mapping['UBL'] = self::YML_BASE_PATH . 'UBL/';
            $this->jmsSerializer = $this->build($mapping, self::ROOT_MAP_UBL);
        }
        return $this->jmsSerializer;
    }

    protected function getRootNodeMap() : array
    {
        return self::ROOT_NODE_MAP;
    }

}
