#!/bin/bash
function xsd2php () {
    rm -rf generated
    ../../../goetas-webservices/xsd2php/bin/xsd2php convert ota-xsd-config-ubl.yml ubl/common/*.xsd ubl/maindoc/*.xsd

    patch -t -N -p1 < ../patches/composer/bobbie-ubl-fix-incorrect-types.patch
}

set -eu
if [[ -d generated ]] ; then
    cd ..
    if [[ -n $(find . -cnewer schemas/generated -not -path "./schemas/generated/*"  -not -path "./.git/*" ) ]] ; then
        echo "Found changed files, regenerating"
        cd schemas
        xsd2php
    else
        echo "Found no changed files, skipping generation"
    fi
else
    xsd2php
fi

