#!/bin/bash
set -eu
grep -r namespace= ubl | cut -d\" -f 2 > namespace-list.tmp1
grep -r targetNamespace= ubl > namespace-list.tmp2
REGEX="targetNamespace=\"([a-zA-Z0-9:\-]*)\""
IFS=$'\n'
for f in `cat namespace-list.tmp2`; do
    if [[ $f =~ $REGEX ]] ; then
        echo ${BASH_REMATCH[1]} >> namespace-list.tmp1
    fi
done
rm namespace-list.tmp2
sort namespace-list.tmp1 | uniq > namespace-list-ubl.csv
rm namespace-list.tmp1
